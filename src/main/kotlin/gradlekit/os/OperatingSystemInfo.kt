package gradlekit.os

import java.io.File

interface OperatingSystemInfo {
    /**
     * Decorate the given name with punctuation to make it clear it's an environment variable.
     */
    fun envName(name: String): String

    /**
     * Return the gradlew invocation appropriate for the platform ("./gradlew" or "gradlew.bat")
     */
    @Suppress("SpellCheckingInspection")
    val gradlew: String

    /**
     * Return the file extension that ordinary dynamic libraries use on this OS
     */
    val dll: String

    /**
     * Return the file extension that Python uses for dynamic libraries on this OS
     */
    val pyd: String get() = dll

    /**
     * Return the first path entry for the given name
     */
    fun which(programName: String): File? = PATH
        .flatMap { PATHEXT.map { ext -> File(it, "$programName$ext")} }
        .firstOrNull { it.isFile && it.canExecute() }

    /**
     * Given a map of operating system names to values, choose the value that is the best fit for
     * this operating system.
     *
     * The supported keys are "windows", "mac", "linux", "unix", and "other", with the most-specific
     * choice being preferred.
     *
     * For example, Linux will return options["linux"] ?: options["unix"] ?: options["other"].
     *
     * This is a convenience method for groovy, which does not have kotlin's `when` statement.
     */
    fun <T> pick(options: Map<String, T>): T?

    /**
     * Return a list of globs to search for Java installation files.
     */
    val javaPaths: List<String>

    /**
     * System path, split out as a list of strings.
     */
    @Suppress("PropertyName")
    val PATH get() = System.getenv("PATH")?.split(File.pathSeparator) ?: emptyList()

    /**
     * List of file extensions the OS considers executable/magic. On Windows, this is usually .exe, .bat, etc.
     * On other platforms, it's `listOf("/")`.
     */
    @Suppress("PropertyName", "SpellCheckingInspection")
    val PATHEXT: List<String>

    /**
     * Return the subset of system drive letters that are referenced by the PATH, which hopefully will include the OS
     * drive but not any CD/floppy drives. On Unixes, this is just `listOf("/")`
     */
    val pathRoots get() = File.listRoots().filter { f -> PATH.any { it.startsWith(f.absolutePath) } }

    /**
     * Return a list of [File]s that match the given glob [pattern] relative to the given [baseFolder]. There is no
     * Java-standard implementation of glob, and although Gradle implements one itself, it turns out to be virtually
     * impossible for third-party code to leverage it.
     */
    fun glob(pattern: String, baseFolder: File): List<File> {
        val patternParts = pattern
            .split(Regex("[/\\\\]"))
            .toMutableList()

        val initial = if (patternParts[0].isEmpty()) // glob starts with a unix-style leading slash
            pathRoots
        else if (Regex("^\\w:$").containsMatchIn(patternParts[0])) // glob starts with a windows-style drive letter
            listOf(File("${patternParts.removeAt(0)}\\")).filter { it.isDirectory }
        else // relative path
            listOf(baseFolder)

        return patternParts
            .fold(initial) { places, part ->
                when (part) {
                    "", "." -> places
                    ".."    -> places.map { it.parentFile }
                    else    -> places.filter { it.isDirectory }.let { dirs ->
                        if ("*" in part) {
                            val re = Regex("^${Regex.escape(part).replace("*", "\\E.*\\Q")}$")
                            dirs
                                .flatMap { (it.listFiles() ?: emptyArray()).asIterable() }
                                .filter { re.containsMatchIn(it.name) }
                        } else {
                            dirs.map { File(it, part) }
                        }
                    }
                }
            }
            .filter { it.exists() }
    }

    /**
     * Return a list of [File]s that match the given glob [pattern] relative to the current working directory.
     */
    // Note: this is a separate function because JvmOverloads doesn't work on interfaces, and Groovy can't figure out
    // what to do with optional paremeters otherwise.
    fun glob(pattern: String) = glob(pattern, File("."))
}
