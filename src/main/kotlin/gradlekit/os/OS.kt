package gradlekit.os

import java.io.File

/**
 * Implementations of [OperatingSystemInfo] that are correct for popular operating systems.
 */
sealed class OS: OperatingSystemInfo {
    companion object {
        /**
         * The current operating system, as detected by gradle.
         */
        @JvmStatic
        val current get() = from(org.gradle.internal.os.OperatingSystem.current())

        /**
         * Returns the [OS] represented by the given Gradle internal OperatingSystem object.
         */
        @JvmStatic
        fun from(operatingSystem: org.gradle.internal.os.OperatingSystem) = when {
            operatingSystem.isWindows -> Windows
            operatingSystem.isMacOsX -> Mac
            operatingSystem.isLinux -> Linux
            else -> error("Gradlekit is not yet compatible with the operating system '${operatingSystem.name}'. " +
                          "Please let us know by filing an issue.")
        }
    }

    override fun <T> pick(options: Map<String, T>) = options["other"]
    protected val detectedJavaPaths = System.getProperty("java.home")?.let { listOf("$it/..") } ?: emptyList()

    /**
     * Shared base class for operating systems such as Mac and Linux, which use / as path separators, $ for
     * environment variables, and otherwise behave in a unixy way.
     */
    abstract class UnixLike : OS() {
        override fun envName(name: String) = "\$$name"
        override val dll = ".so"
        override fun  <T> pick(options: Map<String, T>) = options["unix"] ?: super.pick(options)
        override val javaPaths = detectedJavaPaths + listOf("/usr/*/java", "/usr/*/jvm", "/usr/*/jvm/*")
        override val gradlew = "./gradlew"
        override val PATHEXT get() = listOf("")
    }

    /**
     * Supplies [OperatingSystemInfo] values appropriate for Windows.
     */
    object Windows : OS() {
        override fun envName(name: String) = "%$name%"
        override val dll = ".dll"
        override val pyd = ".pyd"
        override fun <T> pick(options: Map<String, T>) = options["windows"] ?: super.pick(options)
        override val PATHEXT get() =
            System.getenv("PATHEXT")?.split(File.pathSeparator)?.takeUnless { it.isEmpty() } ?: listOf("")
        override val javaPaths = detectedJavaPaths.map { "$it/jdk*" } + pathRoots.flatMap {
            listOf("$it\\Program Files\\Java\\*", "$it\\Program Files (x86)\\Java\\*")
        }
        override val gradlew = "gradlew.bat"
    }

    /**
     * Supplies [OperatingSystemInfo] values appropriate for macOS.
     */
    object Mac : UnixLike() {
        override val dll = ".dylib"
        override fun <T> pick(options: Map<String, T>) = options["mac"] ?: super.pick(options)
        override val javaPaths = listOf("/Library/Java/JavaVirtualMachines/*/Contents/Home")
    }

    /**
     * Supplies [OperatingSystemInfo] values appropriate for Linux.
     */
    object Linux: UnixLike() {
        override fun <T> pick(options: Map<String, T>) = options["linux"] ?: super.pick(options)
    }
}
