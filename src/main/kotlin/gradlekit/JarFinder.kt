package gradlekit

import java.io.File
import gradlekit.os.OS
import org.gradle.api.Project
import kotlin.coroutines.experimental.buildSequence

/**
 * A utility for finding jars that can't be downloaded from Maven, searching in locations relative to the project,
 * and in well-known locations in the operating system.
 */
open class JarFinder(val project: Project, val buildFileDir: String?) {
    fun findJar(name: String): File? = listSearchPaths(name)
        .asSequence()
        .flatMap {
            val file = File(it)
            if (file.exists() && file.isFile && it.endsWith(".jar"))
                sequenceOf(file)
            else
                OS.current.glob("$it/$name.jar").asSequence()
        }.firstOrNull()

    fun listSearchPaths(name: String) = buildSequence {
        val pathPropertyName = "${name}Path"
        project.findProperty(pathPropertyName)?.toString()?.let {
            if (!it.endsWith(".jar"))
                error("-P$pathPropertyName=$it is an invalid path (must end in .jar).")
            if (File(it).run { exists() && isFile })
                yield(it)
            else
                error("-P$pathPropertyName=$it is not a file.")
        }
        yield(project.projectDir.absolutePath)
        buildFileDir?.let { yield(it) }
        yield(project.rootProject.projectDir.absolutePath)
        yield(project.rootProject.projectDir.parentFile.absolutePath)

        // System java paths
        yieldAll(OS.current.javaPaths)

        // Paths based on the user's home folder.
        System.getProperty("user.home")?.let {
            yield(it)
            yield("$it/utils")
        }
    }.flatMap {
        sequenceOf(it, "$it/lib", "$it/libs", "$it/jar", "$it/jars")
    }.toList()
}
