package gradlekit

import org.gradle.api.Plugin
import org.gradle.api.Project;


// It initially seemed like a plugin was the answer for PluginKit, however
// it turned out to be easier to use imported gradle files. We may not
// need this in the end.
class GradleKit implements Plugin<Project> {
    void apply(Project project) {
    }
}


