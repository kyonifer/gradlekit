package gradlekit.logic

import org.gradle.api.Project
import org.gradle.api.initialization.dsl.ScriptHandler

interface MultiplatformBuildLogic {
    void runLogic(String platform, Project project)

    void runBuildscriptLogic(String platform, Object buildscript)

    def mooworkNode = new MultiplatformBuildLogic() {
        @Override
        void runLogic(String platform, Project project) {
            project.with {
                apply{plugin("com.moowork.node")}
            }
        }

        @Override
        void runBuildscriptLogic(String platform, Object buildscript) {
            buildscript.with {
                repositories {
                    maven {
                        url "https://plugins.gradle.org/m2/"
                    }
                }
                dependencies {
                    classpath("com.moowork.gradle:gradle-node-plugin:1.2.0")
                }
            }
        }
    }

    def koma = new MultiplatformBuildLogic() {
        @Override
        void runLogic(String platform, Project project) {
            project.with {
                dependencies {
                    compile "com.kyonifer:koma-core-api-${platform}:${System.getProperty("komaVersion")}"
                    testCompile "com.kyonifer:koma-core-ejml:${System.getProperty("komaVersion")}"
                    if (platform == "jvm") {
                        compile("com.kyonifer:koma-logging:${System.getProperty("komaVersion")}")
                    }
                }
                repositories {
                    jcenter()
                    maven {
                        url "http://dl.bintray.com/kyonifer/maven"
                    }
                }
            }
        }
        @Override
        void runBuildscriptLogic(String platform, Object buildscript) {
        }
    }

    def kotlinxSerialization = new MultiplatformBuildLogic() {
        @Override
        void runLogic(String platform, Project project) {
            project.with {

                apply plugin: "kotlinx-serialization"
                repositories {
                    maven { url "https://kotlin.bintray.com/kotlinx" }
                }
                dependencies {
                    def postfix = platform == "js" ? "-js" : ""
                    compile("org.jetbrains.kotlinx:kotlinx-serialization-runtime${postfix}:0.6.2")
                }
            }
        }

        @Override
        void runBuildscriptLogic(String platform, Object buildscript) {
            buildscript.with {
                repositories {
                    maven { setUrl("https://kotlin.bintray.com/kotlinx") }
                }
                dependencies {
                    classpath("org.jetbrains.kotlinx:kotlinx-gradle-serialization-plugin:0.6.2")
                }
            }
        }
    }
}
