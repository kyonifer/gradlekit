Debugging This Project
----------------------

To debug logic in buildSrc that is not behaving properly during build,
you can perform the following procedure:

1. Open the project in intellij and ensure the import works correctly (i.e. you will need a compiling
    build system when you are getting set up)

2. Run `gradle build -Dorg.gradle.debug=true --no-daemon` in the root build repo (i.e. *not* buildSrc)

3. In intellij, make a run configuration with type `Remote` and ensure
    the classpath is set to buildSrc.

4. Set a debug point and run the new configuration.
